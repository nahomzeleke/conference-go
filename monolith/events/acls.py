import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):

    header = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1

    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params,headers=header)
    content = response.json()

    try:
        return { "picture_url": content["photos"][0]["src"]["original"]}

    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city,state):

    params = {
        "q": city + "," +state,
        "appid": {OPEN_WEATHER_API_KEY},

    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params)
    content = response.json()

    if not content:
        return None
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]

    except (KeyError, ValueError):
        return None

    current_weather_params = {
        "lat": lat,
        "lon": lon,
        "units":"imperial",
        "appid": {OPEN_WEATHER_API_KEY},

    }
    url_2 = "https://api.openweathermap.org/data/2.5/weather"
    response_2 = requests.get(url_2, current_weather_params)
    content_2 = response_2.json()
    if not content:
        return None
    try:
        main_temp = content_2["main"]["temp"]
        description = content_2["weather"][0]["description"]

        return {
            "temp": main_temp,
            "description": description,
        }

    except (KeyError, IndexError):

         return {"weather": None}


def get_fun_fact():
    """This is my docstring."""
    # Use Request lib to get a fun fact
    response = requests.get("https://jservice.xyz/api/random-clue")
    clue = response.json()  # Could have used json.loads(response.content)
    print(clue)
    # Create a dict of the data that we would like to use from the jservice response
    #   question
    #   answer
    #   category
    fun_fact = {
        "question": clue["question"],
        "answer": clue["answer"],
        "category": clue["category"]["title"],
    }

    # Return the dict
    return fun_fact
